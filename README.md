# Setup

This project provides the requirements for the Drupal 7 to Drupal 9 migration training in a minimum number of steps. It makes use of [DrupalVM](https://github.com/geerlingguy/drupal-vm). This is the recommended way to participate in the training, but alternative instructions to not use a virtual machine are available at the end of this README.


## Virtual machine setup

  * If not already installed, download and install [Vagrant](https://www.vagrantup.com/downloads.html) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
  * Ensure you have git; open a command line terminal and type `git --version`.  If you do not get a git version number, [install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
  * `git clone https://gitlab.com/agaric/drupal-upgrade-training.git`
  * `cd drupal-upgrade-training`
  * `vagrant up`

Once this process has completed, verify that you can reach the dashboard URL (should be shown in the output of vagrant up - by default it is http://dashboard.drupal-upgrade-training.test). From there, you can click through to both the Drupal 7 and Drupal 9 sites to ensure they are also functioning properly. The Drupal 7 site should have source content, and the Drupal 9 site should be empty.

If you cannot reach either site, there was likely an error during the vagrant up that should be reviewed/fixed. Note that running `vagrant up` again may fail to retry provisioning if a transient (e.g. network) error cause the issue. You will have to run `vagrant up --provision` to ensure provisioning re-executes. See also [TROUBLESHOOTING.md](TROUBLESHOOTING.md) to see if any issue you have already has a fix.

### Where Drupal & Drush are in the VM

Get into the virtual machine with `vagrant ssh`

### Drupal 7

  * Web root: `/var/www/drupalvm/drupal7`
  * Drush command: `drush8`
  * Login: `drush8 uli`

Note: To interact with Drupal 7 use Drush 8 whose command is `drush8`. Yes, Drupal 7 uses *Drush version 8*.  The current version of Drush, version 10, does not work with Drupal 7. Drush 8 commands for Drupal 7 must be run from Drupal 7's web root folder at `/var/www/drupalvm/drupal7`.

### Drupal 9:

  * Web root: `/var/www/drupalvm/drupal9`
  * Drush command: `drush`
  * Login: `drush uli`

Note: To interact with Drupal 9 use Drush 10 whose command is `drush`. Drush 10 commands for Drupal 9 must be run from Drupal 9's web root folder at `/var/www/drupalvm/drupal9` or a subfolder like ``/var/www/drupalvm/drupal9/web``.

### Helpful commands

Import the configuration for changes to the import module:

`drush config-import --partial --source=modules/custom/ud_drupal_upgrade/config/install`


## DIY requirements

A Drupal 7 site, with the following modules available (this database will be re-loaded from our sample source):

```
addressfield ctools date entity entityreference email link telephone token url pathauto views
```

A Drupal 8/9 site, managed by composer, with the following modules:

```
composer require "drupal/devel:^2.1"
composer require "drupal/address:^1.8"
composer require "drupal/migrate_plus:^5.1"
composer require "drupal/migrate_tools:^5.0"
composer require "drupal/migrate_upgrade:^3.2"
composer require "drupal/paragraphs:^1.12"
composer require "drush/drush:^10"
```

### Repositories:

  * https://gitlab.com/agaric/drupal-upgrade-training
  * https://github.com/dinarcon/ud_drupal_upgrade


## Shared notes

Put your own tips and errors or other problems you run into here!

https://pad.drutopia.org/p/agaric-drupal-upgrade-training


# Troubleshooting

Solutions to known issues are documented in [TROUBLESHOOTING.md](TROUBLESHOOTING.md)