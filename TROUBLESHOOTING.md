# Troubleshooting

## General guidance

Until you are able to reach the dashboard and Drupal 7 and 9 sites as described in the [README.md](README.md), you should presume the "provisioning" did not complete. After trying any of these steps, you will likely need to re-run the provisioning steps with `vagrant up --provision`

## Mac OS

### NFS-related issues

Possible errors/symptoms:

* `playbook` does not exist on the guest: /vagrant/box/provisioning/playbook.yml
* Inability to list files in /vagrant (e.g. `vagrant ssh`, then `ls /vagrant` results in "Stale file handle")

This issue has been specifically seen on Catalina, which includes an issue with additional permissions that Mac applies to certain user folders (e.g. Desktop, Downloads, Documents). If your project folder is under one of these, you have two options:

* Clone the project into a new folder under your user folder. For example, under /Users/john/Projects, rather than /Users/john/Documents/. Such folders should not be specially protected by the OS.
* The other option is to ensuring that the nfsd service (NFS file sharing server, that is used to share files with the virtual machine) is given special access to the folder. [An article by Leonid on the Docksal blog](https://blog.docksal.io/nfs-access-issues-on-macos-10-15-catalina-75cd23606913)  describes this process.

If this does not appear to be the issue you are having, then:

* Ensure all the dependencies for the Virtual Machine are up-to-date (e.g. vagrant, VirtualBox)
* Make sure there are no invalid entries in your exports file. This file controls what the NFS server "shares" out, and if it includes folders that e.g. no longer exist, this may cause an issue as well. Lines can be removed from the file if you know they are not needed, or they may be commented/made inactive by placing a # at the front of the line. You can edit the NFS exports file with: `sudo nano /etc/exports`

## Windows

### NFS not installed

Error message:
```
It appears your machine doesn't support NFS, or there is not an
adapter to enable NFS on this machine for Vagrant. Please verify
that `nfsd` is installed on your machine, and try again. If you're
on Windows, NFS isn't supported. If the problem persists, please
contact Vagrant support.
```

While there are options for running NFS on Windows, the simplest option is to attempt switching to SMB for file sharing. Edit the file in `drupal-upgrade-training\provisioning\config.yml` and change the line for the default folder sharing type from NFS to SMB:

```
vagrant_synced_folder_default_type: smb
```
If you are unable to get SMB working, you may use VirtualBox's built-in file sharing, by making the type blank:

```
vagrant_synced_folder_default_type:
```

Additional information about file sharing in Windows may be found in the [DrupalVM docs](http://docs.drupalvm.com/en/latest/getting-started/syncing-folders/).

### Error running `load_d7.sh`

If your git configuration does not preserve the source repository line endings, you may have this issue. This is due to to Windows having different line ending values compared to Unix/Linux/Mac. If you use a text editor that supports the conversion, simply re-save the file with it, specifying Unix-style line endings (i.e. LF only, rather than CR+LF). Otherwise, you may correct the file within the VM as follows (from a command prompt):

```
cd {your drupal-training folder}
vagrant ssh
cd /var/www/drupalvm
tr -d '\r' < load_d7.sh > tmp.sh && mv tmp.sh load_d7.sh
```

## GNU/Linux

### NFS not installed

Error message:
```
It appears your machine doesn't support NFS, or there is not an
adapter to enable NFS on this machine for Vagrant. Please verify
that `nfsd` is installed on your machine, and try again. If you're
on Windows, NFS isn't supported. If the problem persists, please
contact Vagrant support.
```

As stated, you will need to install an nfs server. On Ubuntu (or most any recent Debian-based system, e.g. Mint), the following should install NFS server to fix the error:

```
sudo apt update
sudo apt install nfs-kernel-server
```

For Arch based systems, you need nfs-utils (`pacman -S nfs-utils`). [Arch wiki - NFS](https://wiki.archlinux.org/index.php/NFS#Server)

### NFS-locking (delete web/core/.nfsXXXXXXXXXXX error)

If you experience locking issues (unable to delete web/core/.nfsXXXXXXXXXXX while composer is running), you may want to try the following:

* Log into the Vagrant VM: `vagrant ssh`
* Show the existing NFS mounted volumes: `mount -t nfs`
* For example, two lines similar to this should be displayed:

```
192.168.88.1:/home/myname/projects/drupal-upgrade-training on /var/www/drupalvm type nfs (rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=192.168.88.1,mountvers=3,mountport=56297,mountproto=udp,local_lock=none,addr=192.168.88.1)

192.168.88.1:/home/myname/projects/drupal-upgrade-training on /vagrant type nfs (rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=192.168.88.1,mountvers=3,mountport=56297,mountproto=udp,local_lock=none,addr=192.168.88.1)
```

In particular, note the `192.168.88.1:/home/myname/projects/drupal-upgrade-training` - the location of the NFS share, from the perspective of the Vagrant machine - referred to below as the `<nfs_share>`.
* Let's remount those, with reduced client-side caching of attributes. We're basically adding the `actimeo=2` option to the list of options (as discovered by the `mount` command above). The options used are all the items inside the parenthesis, which we can provde to `mount` command with the `-o` option. We're also adding `remount` to the beginning of the list of options, to ensure mount will apply the changes we're adding. Remember to substitute the above value for `<nfs_share>` in these commands: `sudo mount -t nfs -o remount,rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=192.168.88.1,mountvers=3,mountport=56297,mountproto=udp,local_lock=none,addr=192.168.88.1,actimeo=2 <nfs_share> /var/www/drupalvm/`
* Do the same for `/vagrant`: `sudo mount -t nfs -o remount,rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=192.168.88.1,mountvers=3,mountport=56297,mountproto=udp,local_lock=none,addr=192.168.88.1,actimeo=2 <nfs_share> /vagrant/`
* Still in the VM, you can try running composer with these changes applied. This should now get the source installed, so that the provision will skip it next time: `cd /var/www/drupalvm/drupal9 && composer install`
* Given the provisioning still has some work to do, we'll have to continue that. Exit the VM (`ctrl+d` or `exit`), and then run `vagrant provision`

At this point, the provision should complete successfully.

## Non-OS specific

### Issues with ansible installed on host (python error)

Some issues, such as a python environment issue, or possibly other issues running ansible on the host machine can be fixed by using ansible in the host machine. An example error might be:

```
fatal: [drupal-upgrade-training-vm -> localhost]: FAILED! => {"changed": false, "module_stderr": "/bin/sh: /usr/bin/python3: No such file or directory\n", "module_stdout": "", "msg": "The module failed to execute correctly, you probably need to set the interpreter.\nSee stdout/stderr for the exact error", "rc": 127}
```

In this case, you can just try setting the location of your python executable. Adding the ansible_python_interpreter variable to the vagrant config.yml file will do this. In `provisioning/config.yml`, add:
```
ansible_python_interpreter: /usr/bin/python3
```
Rather than exactly `/usr/bin/python3`, point to the actual location of your python executable.

For other issues (also works for above issue) you can modify the file `provisioning/config.yml` and change the value for `force_ansible_local` to `true` (defaul `false`).

This forces the VM to provision by installing ansible inside of the VM, rather than using one that is installed on the host.
